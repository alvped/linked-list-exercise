package com.example.collections;

/**
 * Implements a Doubly Linked List. It offers functionality for add an element
 * at the end of the list, add an element at a specific index keeping the
 * relative order of the other elements, push an element as first in the list,
 * get the size of the list and get an element at a specific index.
 *
 * @param <E>
 */
public class DoublyLinkedList<E> {

	private Node<E> head;

	public void add(E element) {
		if (head == null) {
			push(element);
		} else {
			append(element);
		}
	}

	public void add(int index, E element) {
		if (index == 0) {
			push(element);
		} else if (index > 0 && index < size()) {
			insertBeforeCurrentNodeInIndex(index, element);
		} else if (index == size()) {
			append(element);
		} else {
			throw new IndexOutOfBoundsException();
		}
	}

	public int size() {
		int size = 0;
		if (head != null) {
			size = 1;
			Node<E> currentNode = head;
			while (currentNode.getNext() != null) {
				size++;
				currentNode = currentNode.getNext();
			}
		}
		return size;
	}

	public void push(E element) {
		Node<E> newNode = new Node<>(element);
		insertBefore(head, newNode);
		head = newNode;
	}

	public E get(int index) {
		if (index < 0) {
			throw new IndexOutOfBoundsException();
		}
		return index == 0 ? getValueFrom(head) : getValueFrom(traverseListTo(index));
	}

	private void append(E element) {
		Node<E> newNode = new Node<>(element);
		Node<E> lastNode = findLast();
		insertAfter(lastNode, newNode);
	}

	private void insertBeforeCurrentNodeInIndex(int index, E element) {
		Node<E> currentNodeInIndex = traverseListTo(index);
		Node<E> currentNodeInPreviousIndex = traverseListTo(index - 1);
		Node<E> newNode = new Node<>(element);

		insertAfter(currentNodeInPreviousIndex, newNode);
		insertBefore(currentNodeInIndex, newNode);
	}

	private Node<E> findLast() {
		Node<E> lastNode = head;
		while (lastNode.getNext() != null) {
			lastNode = lastNode.getNext();
		}
		return lastNode;
	}

	private void insertBefore(Node<E> node, Node<E> newNode) {
		newNode.setNext(node);
		if (node != null) {
			node.setPrevious(newNode);
		}
	}

	private void insertAfter(Node<E> node, Node<E> newNode) {
		node.setNext(newNode);
		newNode.setPrevious(node);
	}

	private Node<E> traverseListTo(int index) {
		Node<E> currentNode = head;
		for (int i = 0; i < index; i++) {
			if (currentNode.getNext() != null) {
				currentNode = currentNode.getNext();
			} else {
				throw new IndexOutOfBoundsException();
			}
		}
		return currentNode;
	}

	private E getValueFrom(Node<E> node) {
		if (node == null) {
			throw new IndexOutOfBoundsException();
		}
		return node.getValue();
	}
}
