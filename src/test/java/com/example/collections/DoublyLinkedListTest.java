package com.example.collections;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import mockit.Deencapsulation;

public class DoublyLinkedListTest {
	
	private DoublyLinkedList<String> list;

	@Before
	public void setUp() throws Exception {
		list = new DoublyLinkedList<>();
	}
	
	@Test
	public void testSize_emptyList() {
		assertEquals("Empty list should have size = 0",  0 , list.size());
	}
	
	@Test
	public void testSize_oneELement() {
		list.add("");
		
		assertEquals("List with 1 element should have size=1",  1 , list.size());
	}
	
	@Test
	public void testSize_threeElements() {
		list.add("");
		list.add("");
		list.add("");
		
		assertEquals("List with 3 elements should have size=3 ",  3 , list.size());
	}
	
	@Test
	public void testAdd() {
		String firstAddedElement = "elemento 1";
		String secondAddedElement = "elemento 2";
		
		list.add(firstAddedElement);
		list.add(secondAddedElement);

		assertEquals(firstAddedElement, list.get(0));
		assertEquals(secondAddedElement, list.get(1));
	}
	
	@Test
	public void testAdd_indexZeroWithemptyList() {
		list.add(0, "elemento 1");
		assertEquals("elemento 1", list.get(0));
	}
	
	@Test
	public void testAdd_indexZeroNonEmptyList() {
		list.add(0, "elemento 1");
		list.add(0, "elemento 2");
		assertEquals("elemento 2", list.get(0));
		assertEquals("elemento 1", list.get(1));
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void testAdd_indexBiggerThanListSize() {
		list.add("element 1");
		list.add(7, "elemento");
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void testAdd_indexNegative() {
		list.add(-7, "elemento");
	}
	
	@Test
	public void testAdd_indexEqualsThanSize() {
		list.add("element 1");
		list.add("element 2");
		
		list.add(2, "elemento 3");
		
		assertEquals("If index equals than size, then element "
				+ "should be appended at the end of the list", 
				"elemento 3", list.get(2));
	}
	
	@Test
	public void testAdd_indexBetweenTwoNodes() {
		list.add("element 1");
		list.add("element 2");
		list.add("element 3");
		
		list.add(1, "element 4");
		
		assertEquals("element 1", list.get(0));
		assertEquals("element 4", list.get(1));
		assertEquals("element 2", list.get(2));
		assertEquals("element 3", list.get(3));
	}
	
	@Test
	public void testPush() {
		Deencapsulation.setField(list, "head", new Node<>("elemento 1"));
		list.push("elemento 2");
		assertEquals("elemento 2", list.get(0));
	}
	
	public void testPushThenAdd() {
		list.push("elemento 1");
		list.add("elemento 2");

		assertEquals("elemento 1", list.get(0));
		assertEquals("elemento 2", list.get(1));
	}
	
	public void testAddThenPush() {
		list.add("elemento 1");
		list.push("elemento 2");

		assertEquals("elemento 2", list.get(0));
		assertEquals("elemento 1", list.get(1));
	}
	
	@Test
	public void testPush_swapHeadWithNewElement() {
		list.push("elemento 1");
		list.push("elemento 2");
		
		assertEquals("elemento 2", list.get(0));
		assertEquals("elemento 1", list.get(1));
	}
	
	@Test
	public void testPush_keepsOrderBetweenInsertions() {
		list.push("elemento 1");
		list.push("elemento 2");
		list.push("elemento 3");
		
		assertEquals("elemento 3", list.get(0));
		assertEquals("elemento 2", list.get(1));
		assertEquals("elemento 1", list.get(2));
	}
	
	@Test
	public void testGet() {
		list.add("elemento 1");
		list.add("elemento 2");
		
		assertEquals("elemento 1", list.get(0));
		assertEquals("elemento 2", list.get(1));
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void testGet_indexOutOfBoundsWhenEmpty(){
		list.get(0);
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void testGet_indexOutOfBoundsWhenIndexNegative(){
		list.add("elemento 1");
		list.get(-3);
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void testGet_indexOutOfBoundsWhenIndexHigherThanAllowed() {
		list.push("elemento 1");
		list.push("elemento 2");
		list.push("elemento 3");
		
		list.get(3);
	}
}
